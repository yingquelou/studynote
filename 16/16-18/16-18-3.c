#include <stdio.h>
#include <math.h>
typedef struct
{
    float angle;
    float modulus;
} polar;
typedef struct
{
    float x;
    float y;
} Cxy;
Cxy showCxy(polar);
int main(void)
{
    polar p = {M_PI / 3, 2.0};
    Cxy tmp;
    tmp = showCxy(p);
    printf("(%.2f,%.2f)=>>(%.2f,%.2f)\n", p.angle, p.modulus, tmp.x, tmp.y);
    return 0;
}
Cxy showCxy(polar p)
{
    Cxy tmp;
    tmp.x = p.modulus * cos(p.angle);
    tmp.y = p.modulus * sin(p.angle);
    return tmp;
}