#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#define SIZE 10
void roll(int[], int, int);
int main(void)
{
    int arr[SIZE] = {2, 7, 8, 1, 3, 4, 5, 9, 45, 6};
    roll(arr, SIZE, 3);
    return 0;
}
void rm(int *arr, int size, int i)
{
    for (; i < size - 1; i++)
        arr[i] = arr[i + 1];
}
void roll(int arr[], int size, int count)
{
    if (count > size)
        return;
    srand((unsigned)time(NULL));
    int tmp;
    do
    {
        tmp = rand() % size;
        if (arr[tmp])
        {
            count--;
            printf("%d ", arr[tmp]);
            rm(arr, size, tmp);
            --size;
        }
    } while (count > 0);
    puts("");
}