#include <stdio.h>
#include <time.h>
void latency(double);
int main(void)
{
    for (size_t i = 0; i < 10; ++i)
    //每隔一秒打印一次1
    {
        latency(1.0);
        printf("1");
    }
    puts("");
    return 0;
}
void latency(double time)
{
    static double start;
    start = (double)clock();
    while (((double)clock() - start) / CLOCKS_PER_SEC < time)
        continue;
}