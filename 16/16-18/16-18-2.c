#include <stdio.h>
#define HARM(X, Y) (1.0 / ((1.0 / (X) + 1.0 / (Y)) / 2))
int main(void)
{
    printf("The harmonic mean of %.2f and %.2f is %.2f.\n",
           2.0, 3.0, HARM(2, 3));
    return 0;
}