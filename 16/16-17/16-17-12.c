#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#define PN puts("")
#define SIZE 100
int comp(const void *, const void *);
int main(void)
{
    srand((unsigned)time(NULL));
    int scores[SIZE];
    unsigned i = 0;
    for (; i < SIZE; i++)
    {
        scores[i] = rand() % SIZE;
        printf("%d ", scores[i]);
    }
    PN;
    qsort(scores, SIZE, sizeof(int), comp);
    for (i = 0; i < SIZE; i++)
        printf("%d ", scores[i]);
    PN;
    return 0;
}
int comp(const void *e1, const void *e2)
{
    return *(int *)e2 - *(int *)e1;
}