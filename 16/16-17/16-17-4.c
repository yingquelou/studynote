#include <stdio.h>
#define EVEN_GT(X, Y) (((X) > (Y) && (X) % 2 == 0) ? 1 : 0)
int main(void)
{
    if (EVEN_GT(4 + 2, 1 + 3))
        puts("Hello World!");
    return 0;
}