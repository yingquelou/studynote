#include <stdio.h>
#include <stdbool.h>
#define BOOL(X) _Generic( \
    X, _Bool              \
    : "boolean",          \
      default             \
    : "not boolean");
int main(void)
{
    puts("Hello World!");
    return 0;
}