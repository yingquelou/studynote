#include <stdio.h>
#define PRINT_INT_VA(X) printf("name: " #X "; value: %d; address: %p\n", X, &X)
//#define _TEST_
#define PR_DATE
int main(void)
{
#ifdef _TEST_
    int fop = 23;
    PRINT_INT_VA(fop);
#endif
#ifdef PR_DATE
    printf("%s\n", __DATE__);
#endif
    return 0;
}