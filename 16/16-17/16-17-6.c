#include <stdio.h>
#define NUM 25
#define SPACE " "
#define PS printf(SPACE)
#define BIG(X) ((X) + 3)
#define SUMSQ(X, Y) ((X) * (X) + (Y) * (Y))
int main(void)
{
    PS;
    printf("%d\n", SUMSQ(NUM, BIG(1)));
    return 0;
}