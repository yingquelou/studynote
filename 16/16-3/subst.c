/* subst.c -- 在字符串中替换 */
#include <stdio.h>
#define PSQR(x) printf("The square of \"" #x "\" is %.2e.\n", ((x) * (x)))

int main(void)
{
    float y = 5;

    PSQR(y);
    PSQR(12.0 - 2.0);

    return 0;
}