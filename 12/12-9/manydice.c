#include <stdio.h>
#include <stdlib.h>   /* 为库函数 srand() 提供原型    */
#include <time.h>     /* 为 time() 提供原型            */
#include "diceroll.h" /* 为roll_n_dice()提供原型，为roll_count变量提供声明 */
int main(void)
{
    //声明并定义骰子的个数与每个骰子的面数
    int dice, sides;
    //声明并定义每次投掷出的点数与投掷的次数
    int roll, sets;
    srand((unsigned int)time(0)); /* 随机种子 */
    printf("Enter the number of sets; enter q to stop:");
    while (scanf("%d", &sets) == 1 && sets > 0)
    {
        printf("How many sides and how many dice?");
    again:
        while (scanf("%d %d", &sides, &dice) != 2)
        //直到输入正确才跳出循环
        {
            clean_up(); //如果没这一行，输入错误后直接死循环
            printf("Input error!please enter the number of sides and dice,again?\n");
        }
        if (!(sides > 1 && dice > 0))
        //思考:如果不用goto,这个if块能和上面的while块合并吗？
        {
            printf("Input error!please enter the number of sides and dice,again?\n");
            goto again;
        }
        clean_up(); /* 处理多余的输入,以免干扰下一次外循环 */
        unsigned width = 0, temp = sides * dice;
        for (; temp != 0; width++)
            temp /= 10; //产生的width用于输出对齐
        printf("Here are %d sets of %d %d-sided throws.\n", sets, dice, sides);
        for (int count = 0; count < sets; count++)
        {
            roll = roll_n_dice(dice, sides);
            printf("%*d ", width, roll); //width用在这儿
            if (14 == count % 15)
                puts(""); //打印15个后换行
        }
        printf("\nHow many sets? Enter q to stop:");
    }
    //如果要完全按照题目要求，可以去掉下面两个printf
    printf("The rollem() function was called %d times.\n",
           roll_count); //使用外部变量
    printf("GOOD FORTUNE TO YOU!\n");
    return 0;
}