#include <stdio.h>
#include <stdlib.h>
typedef enum mon
{
    January = 1,
    February,
    March,
    April,
    May,
    June,
    July,
    August,
    September,
    October,
    November,
    December
} emon;
typedef struct month
{
    char moname[4];
    unsigned char numofday;
    emon senum;
} MON;
int main(void)
{
    MON arr[12] = {
        {"Jan", 31, 1},
        {"Feb", 28, 2},
        {"Mar", 31, 3},
        {"Apr", 30, 4},
        {"May", 31, 5},
        {"Jun", 30, 6},
        {"jul", 31, 7},
        {"Aug", 31, 8},
        {"Sep", 30, 9},
        {"Oct", 31, 10},
        {"Nov", 30, 11},
        {"Dec", 31, 12}};
    int i = 0;
    while (((scanf("%d", &i) == 1) + (i > 0) + (i < 13)) == 3)
    {
        int tal = 0;
        for (; arr[i-1].senum >0; i--)
            tal += arr[i-1].numofday;
        printf("%d \n",tal);
    }
    return 0;
}