#include <stdio.h>
#include <stdlib.h>
#define count 100
int main(int argc, char *argv[])
{
    if (argc != 3)
    {
        fprintf(stderr, "please tell me two filenames!\nIt's a soucefile and a destfile.\n");
        exit(EXIT_FAILURE);
    }
    FILE *sou, *dest;
    if (!(sou = fopen(argv[1], "rb")))
    {
        fprintf(stderr, "fail to open the soucefile %s\n", argv[1]);
        exit(EXIT_FAILURE);
    }
    if (!(dest = fopen(argv[2], "wb")))
    {
        fprintf(stderr, "fail to open the soucefile %s\n", argv[2]);
        exit(EXIT_FAILURE);
    }
    char temp[count] = {0};
    while (fread(temp, sizeof(char), count, sou))
    fwrite(temp, sizeof(char), count, dest);
    fclose(sou);
    fclose(dest);
    return 0;
}