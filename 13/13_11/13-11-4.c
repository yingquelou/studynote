#include <stdio.h>
#include <stdlib.h>
int main(int argc, char *argv[])
{
    int i;
    if (argc < 2)
    {
        fprintf(stderr, "please a filename at least!\nWe will show their content\n");
        exit(EXIT_FAILURE);
    }
    FILE *fp = NULL;
    for (i = 1; i < argc; i++)
    {
        if (!(fp = fopen(argv[i], "rb")))
        {
            fprintf(stderr, "fail to open the file %s\n", argv[i]);
            exit(EXIT_FAILURE);
        }
        setvbuf(fp, NULL, _IOFBF, BUFSIZ);
        char tmp[1];
        fprintf(stdout, "The following is the file %s\n----------\n", argv[i]);
        while (fread(tmp, sizeof(char), 1, fp))
            fputc(*tmp, stdout);
        fclose(fp);
        fp = NULL;
        puts("");
    }
    puts("Done!");
    return 0;
}