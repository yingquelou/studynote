#include <stdio.h>
#include <string.h>
#include <stdlib.h> // 提供 exit()的原型
#define size 40
int main(void)
{
    int ch;   // 读取文件时，存储每个字符的地方
    FILE *fp; // “文件指针”
    unsigned long count = 0;
    char fname[size] = "";
    if (scanf("%s", fname) != 1)
    {
        printf("haven't filename\n");
        exit(EXIT_FAILURE);
    }
    printf("%d\n",strlen(fname));
    if ((fp = fopen(fname, "r")) == NULL)
    {
        printf("Can't open %s\n",fname);
        exit(EXIT_FAILURE);
    }
    while ((ch = getc(fp)) != EOF)
    {
        putc(ch, stdout); // 与 putchar(ch); 相同
        count++;
    }
    fclose(fp);
    printf("\nFile %s has %lu characters\n",fname, count);
    return 0;
}