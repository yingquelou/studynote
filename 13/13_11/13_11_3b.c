#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#define size 40
//源文件与目标文件的文件名处理
static void getfname(char *const, char *const, int);
int main(void)
{
    char source[size] = {0}, dest[size] = {0};
    getfname(source, dest, size);
    FILE *fps = NULL, *fpd = NULL;
    char tmp[size];
    size_t count;
    if (!((fps = fopen(source, "rb")) && (fpd = fopen(dest, "wb"))))
    //打开失败退出程序
    {
        fprintf(stderr, "fail to open file\n");
        exit(EXIT_FAILURE);
    }
    setvbuf(fps,NULL,_IOLBF,BUFSIZ);
    while (count = fread(tmp, sizeof(char), size, fps))
    {
        size_t i = 0;
        for (; i < count; ++i)
            if (islower(tmp[i]))
                tmp[i] = toupper(tmp[i]);
        fwrite(tmp, sizeof(char), count, fpd);
    }
    return 0;
}
void getfname(char *const source, char *const dest, int n)
{
    puts("please enter a filename!");
    for (size_t i = 0; i < n - 1; i++)
    {
        source[i] = getchar();
        if (source[i] == '\n')
        {
            source[i] = '\0';
            break;
        }
    }
    source[n - 1] = '\0';
    char *pos = &dest[0];
    strcpy(dest, source);
    size_t len = strlen(dest);
    if (pos = strchr(dest, '.'))
    {
        if (len < n - 1)
            for (size_t i = 0; i < &dest[len] - pos; i++)
                dest[len - i] = dest[len - 1 - i];
        *pos = '1';
    }
    else
    {
        if (len < n - 1)
            strcat(dest, "1");
        else
            dest[n - 2] = '1';
    }
    //puts(dest);
}