#include <stdio.h>
#include <stdlib.h>
int main(int n, char **file)
{
    if (n < 1)
        exit(EXIT_FAILURE);
    float in = 0, tol = 0, count = 0;
    if (1 == n)
        while (fscanf(stdin, "%f", &in) == 1)
        {
            count++;
            tol += in;
        }
    else
    {
        FILE *fp = NULL;
        if (!(fp = fopen(file[1], "r")))
        {
            fprintf(stderr, "errer to open %s", file[1]);
            exit(EXIT_FAILURE);
        }
        while (fscanf(fp, "%f", &in) == 1)
        {
            count++;
            tol += in;
        };
        fclose(fp);
    }
    printf("It's %.2f\n", tol / count);
    return 0;
}