#include <stdio.h>
#include <stdlib.h>
int main(int n, char **str)
{
    if (n < 3)
    {
        fprintf(stderr, "commend less\n");
        exit(EXIT_FAILURE);
    }
    FILE *fp = NULL;
    if (!(fp = fopen(str[2], "r")))
    {
        fprintf(stderr, "fail to open the file %s", str[2]);
        exit(EXIT_FAILURE);
    }
    unsigned line = 1;
    int ch = 0;
    while ((ch = getc(fp)) != EOF)
    {
        static unsigned i = 1;
        if (*str[1] == ch)
        {
            printf("There is a '%c' at line %u num %u\n", *str[1], line, i);
            i++;
        }
        if (ch == '\n')
        {
            line++;
            i = 1;
        }
    }
    fclose(fp);
    return 0;
}