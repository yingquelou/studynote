#include <stdio.h>
int tellbit(int);
int main(void)
{
     printf("%d\n", tellbit(-10));
     return 0;
}
int tellbit(int num)
{
     int i = 0, count = 0;
     for (; i < sizeof(int) * 8; i++)
     {
          if ((num >> i) & 1)
               count++;
     }
     return count;
}