#include <stdio.h>
int showbitstatus(const int num, const int pos);
int main(void)
{
     printf("%d\n", showbitstatus(10, 1));
     return 0;
}
int showbitstatus(const int num, const int pos)
{
     return (num >> (pos - 1)) & 1;
}