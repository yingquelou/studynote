#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
unsigned strtonum(const char *);
void showinbin(unsigned);
int main(int argc, char **argv)
{
     if (argc != 3)
     {
          fprintf(stderr, "please enter two binary number.\n");
          exit(EXIT_FAILURE);
     }
     unsigned tmp1 = strtonum(argv[1]);
     unsigned tmp2 = strtonum(argv[2]);
     showinbin(tmp1);
     showinbin(~tmp1);
     showinbin(tmp2);
     showinbin(~tmp2);
     showinbin(tmp2 & tmp1);
     showinbin(tmp2 | tmp1);
     showinbin(tmp2 ^ tmp1);
     return 0;
}
void showinbin(unsigned num)
{
     printf("The binary number of \"%u\" is ", num);
     while (num != 0)
     {
          printf("%u", num % 2u);
          num /= 2u;
     }
     printf("\n");
}
unsigned strtonum(const char *str)
{
     unsigned len = strlen(str), num = 0, i;
     for (i = 0; i < len; i++)
          if (!isdigit(str[i]))
          {
               fprintf(stderr, "The \"%s\" isn't a binary number.\n", str);
               exit(EXIT_FAILURE);
          }
          else if (!(str[i] >= '0' && str[i] < '2'))
          {
               fprintf(stderr, "The \"%s\" isn't a binary number.\n", str);
               exit(EXIT_FAILURE);
          }
     for (i = len - 1; i >= 0; --i)
     {
          num += str[i] - '0';
          if (i)
               num <<= 1;
          if (!i)
               break;
     }
     return num;
}