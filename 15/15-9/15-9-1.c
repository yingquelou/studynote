#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
int strtonum(const char *);
int main(void)
{
     char *pbin = "01001001";
     printf("%d\n", strtonum(pbin));
     return 0;
}
int strtonum(const char *str)
{
     int len = strlen(str), num = 0, i;
     for (i = 0; i < len; i++)
          if (!isdigit(str[i]))
          {
               fprintf(stderr, "The \"%s\" isn't a binary number.\n", str);
               exit(EXIT_FAILURE);
          }
          else if (!(str[i] >= '0' && str[i] < '2'))
          {
               fprintf(stderr, "The \"%s\" isn't a binary number.\n", str);
               exit(EXIT_FAILURE);
          }
     for (i = len - 1; i > 0; --i)
     {
          num += str[i] - '0';
          if (i != 1)
               num <<= 1;
     }
     return num;
}