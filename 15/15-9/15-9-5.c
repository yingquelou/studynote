#include <stdio.h>
unsigned leftrun(const unsigned, const unsigned);
int main(void)
{
     unsigned test = -1 >> 24 << 24;
     //test ==> 1111 1111 0000 0000 0000 0000 0000 0000
     //         0000 0000 0000 0000 0000 0000 1111 1111
     printf("%u\n%u\n", test, leftrun(test, 8));
     return 0;
}
unsigned leftrun(const unsigned num, const unsigned width)
{
     return (num << width) + (num >> (sizeof(unsigned) * 8 - width));
}