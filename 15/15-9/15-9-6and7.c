#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>
#define MENUSIZE 7
#define CHOICEPOS 9
#define FONTOPTION 7
#define SIZEOPTION 8
#define ALIGNOPTION 10
#define BOLDOPTION 11
#define ITALICOPTION 12
#define UNDERLINEOPTION 13
const char *str[] = {
    "f)change font", "s)change size", "a)change alignment",
    "b)toggle bold", "i)toggle italic", "u)toggle underline",
    "q)quit", "Enter font (0-255): ", "Enter font size (0-127): ",
    "fsabiuq", "Select alignment:\n0)left 1)center 2)right\n",
    "toggle bold:\n0)off 1)on\n", "toggle italic:\n0)off 1)on\n",
    "toggle underline:\n0)off 1)on\n"};
typedef union
{
     struct
     {
          unsigned char id;
          unsigned char size : 7;
          unsigned char align : 2;
          unsigned char B : 1;
          unsigned char I : 1;
          unsigned char U : 1;
     };
     unsigned attribute;
} font;
//显示字体信息,第二个参数为真时按位(运算符)显示，为假是按位字段显示
void showfont(const font *, const bool);
//显示菜单
void showmenu(void);
//获取菜单选项
char getoptions(void);
//调用某个函数
void callchange(void (*)(font *), font *);
void changefont(font *);
void changesize(font *);
void changealign(font *);
void bold(font *);
void italic(font *);
void underline(font *);
void quit(font *);
void (*pfun[])(font *) = {
    changefont, changesize, changealign,
    bold, italic, underline,
    quit};
typedef unsigned mytype;
//按位操作
mytype bitAssignment(mytype, mytype, mytype, mytype);
int main(void)
{
     font a[1], *p = a;
     char choice;
     mytype funnum;
     do
     {
          showfont(p, false);
          showmenu();
          choice = getoptions();
          funnum = strchr(str[CHOICEPOS], choice) - str[CHOICEPOS];
          callchange(pfun[funnum], p);
     } while (funnum < MENUSIZE);
     return 0;
}
mytype bitAssignment(mytype dest, mytype start, mytype width, mytype source)
{
     //例如：1000 1100 (1100 0101) 1001 0110 1001 0110即0x8cc59696
     //变成：1000 1100 (1001 1010) 1001 0110 1001 0110即0x8C9A9696
     //须如此调用bitAssignment(0x8cc59696,17,8,0x9a)
     //其意义是：从0x8cc59696的二进制的第17个bit位(包括17)开始的8个bit位替换为0x9a
     //函数返回值即为所求
     if ((width + start - 1) > (sizeof(mytype) * 8))
     {
          fprintf(stderr, "The bit length you want to change cannot be greater than "
                          "the existing bit length starting at the corresponding position.\n");
          exit(EXIT_FAILURE);
     }
     mytype tmp1 = 1, tmp2;
     if (width != sizeof(mytype) * 8)
          tmp1 = ~(-1 << width);
     else
          tmp1 = ~0;
     if (source > tmp1)
     {
          fprintf(stderr, "The number you specify cannot be greater than "
                          "the bit length you want to change. \n");
          exit(EXIT_FAILURE);
     }
     tmp1 <<= start - 1;                       /*此时tmp1是掩码a*/
     tmp2 = tmp1;                              /*掩码a为tmp2存储*/
     tmp1 |= dest;                             /*此时tmp1为目标的中间态*/
     tmp2 = (~tmp2) | (source << (start - 1)); /*此时tmp2是掩码b*/
     tmp1 &= tmp2;
     return tmp1; /*此时tmp1为目标值*/
}
void CLS(void)
{
     while ('\n' != getchar())
          continue;
}
void callchange(void (*change)(font *), font *p)
{
     change(p);
}
void medium(font *p, const char *string, mytype start, mytype width)
{
     printf(string);
     mytype tmp, tmp1;
     while (scanf("%u", &tmp) != 1)
     {
          CLS();
          fprintf(stderr, "Enter a number of the correct option: ");
          continue;
     }
     if (width != sizeof(mytype) * 8)
          tmp1 = ~(-1 << width);
     else
          tmp1 = ~0;
     if (tmp > tmp1)
     {
          fprintf(stderr, "Enter a number of the correct option!\n");
          exit(EXIT_FAILURE);
     }
     CLS();
     p->attribute = bitAssignment(p->attribute, start, width, tmp);
}
void changefont(font *p)
{
     medium(p, str[FONTOPTION], 1, 8);
}
void changesize(font *p)
{
     medium(p, str[SIZEOPTION], 9, 7);
}
void changealign(font *p)
{
     medium(p, str[ALIGNOPTION], 17, 2);
}
void bold(font *p)
{
     medium(p, str[BOLDOPTION], 19, 1);
}
void italic(font *p)
{
     medium(p, str[ITALICOPTION], 20, 1);
}
void underline(font *p)
{
     medium(p, str[UNDERLINEOPTION], 21, 1);
}
void quit(font *p)
{
     exit(EXIT_SUCCESS);
}
char getletter(char ch)
{
     while (!isalpha(ch = getchar()))
     {
          fprintf(stderr, "please enter one letter of \"%s\"\n", str[CHOICEPOS]);
          CLS();
     }
     CLS();
     return tolower(ch);
}
char getoptions(void)
{
     char ch = getletter(ch);
     while (!strchr(str[CHOICEPOS], ch))
     {
          fprintf(stderr, "please enter one letter of \"%s\"\n", str[CHOICEPOS]);
          ch = getletter(ch);
     }
     return ch;
}
void showmenu(void)
{
     for (mytype i = 0; i < MENUSIZE; i++)
     {
          printf("%-20s", str[i]);
          if (i % 3 == 2 && i != MENUSIZE)
               puts("");
     }
     if (MENUSIZE % 3)
          puts("");
}
void showfont(const font *pfont, const bool choice)
{
     if (choice)
          printf("Attribute code of the font is %#x\n", pfont->attribute);
     else
     {
          printf("\n ID SIZE ALIGNMENT  B   I   U\n");
          printf("%3d%5d%8s%6s%4s%4s\n\n",
                 pfont->id, pfont->size,
                 (pfont->align) ? ((pfont->align == 1) ? "center" : "right") : "left",
                 (pfont->B) ? "on" : "off",
                 (pfont->I) ? "on" : "off",
                 (pfont->U) ? "on" : "off");
     }
}