//  align.c -- 使用 _Alignof 和 _Alignas （C11）
#include <stdio.h>
#include <stdlib.h>
#include <stdalign.h>
int main(void)
{
    double dx;
    char ca;
    char cx;
    double dz;
    char cb;
    char alignas(double) cz;
    printf("char alignment:   %llu\n", alignof(char));
    printf("double alignment: %llu\n", alignof(double));
    printf("&dx: %p\n", &dx);
    printf("&ca: %p\n", &ca);
    printf("&cx: %p\n", &cx);
    printf("&dz: %p\n", &dz);
    printf("&cb: %p\n", &cb);
    printf("&cz: %p\n", &cz);
    return 0;
}